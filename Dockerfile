FROM apache/superset
# Switching to root to install the required packages
USER root
# Example: installing the MySQL driver to connect to the metadata database
# if you prefer Postgres, you may want to use `psycopg2-binary` instead
#RUN pip install psycopg2-binary
RUN pip install psycopg2
# Example: installing a driver to connect to Redshift
# Find which driver you need based on the analytics database
# you want to connect to here:
# https://superset.apache.org/installation.html#database-dependencies
#RUN pip install sqlalchemy-redshift

COPY --chown=superset superset_config.py /app/
ENV SUPERSET_CONFIG_PATH /app/superset_config.py

#RUN superset fab create-admin --username admin --firstname Superset --lastname Admin --email admin@superset.com --password admin
#RUN superset superset db upgrade
#RUN superset superset init

#RUN /bin/bash -c superset fab create-admin --username admin --firstname Superset --lastname Admin --email admin@superset.com --password admin && superset superset db upgrade && superset superset init && run-server.sh

# Switching back to using the `superset` user
USER superset

CMD run-server.sh